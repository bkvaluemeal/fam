FROM golang:alpine
MAINTAINER Justin Willis <sirJustin.Willis@gmail.com>

COPY AUTHORS CHANGELOG.md LICENSE README.md main.go /go/src/fam/

RUN go install -v fam

EXPOSE 80

ENTRYPOINT ["fam"]
