package main

import (
	"fmt"
)

const VERSION = "v0.1.0"

func main() {
	fmt.Printf("Fam %s\n", VERSION)
}
